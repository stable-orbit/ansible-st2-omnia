require 'spec_helper'

describe service('st2') do
  it { should be_running }
end

describe package('st2mistral') do
  it { should be_installed }
end

describe package('st2web') do
  it { should be_installed }
end

describe package('st2chatops') do
  it { should_not be_installed }
end

context 'With st2 authentication configured' do
  describe command('st2 action list -j') do
    its(:stdout) { should include 'core.', 'hubot.' }
  end
  describe command('st2 action execute packs.info pack=chatops') do
    its(:stdout) { should include 'To get the results, execute:' }
  end
  describe command('st2 run packs.load') do
    its(:stdout) { should include 'succeeded: true' }
  end
  describe command('st2 run core.local cmd=whoami') do
    its(:stdout) { should include 'stdout: deploy' }
  end
end

context 'Without st2 authentication configured' do
  describe command('su - st2-unauthenticated -c "st2 action list -j"') do
    its(:stdout) { should include 'ERROR: 401 Client Error: Unauthorized' }
  end
end
