require 'spec_helper'

describe service('hubot') do
  it { should be_running.under('upstart') }
end

# TODO: Come up with a way to confirm ST2 <-> Hubot interaction. Without a working
# Slack token the hubot.post_message action throws an error. With a working
# Slack token it sends a message. Also, since hubot runs as a daemon or in a
# shell repl mode there's no apparent way to get info out of the hubot service
# to confirm a received message.
