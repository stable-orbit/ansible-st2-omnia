require 'spec_helper'

describe command('packer --version') do
  its(:stdout) { should match /\d+\.\d+\.\d+/ }
end
