# ansible-st2-omnia

This role should get you a fully functioning, standalone Stackstorm server with
function chatops via Hubot.

## Alpha Version

Still very much a work in progress. Currently this just gets the basic software working without authentication. This is not ready for use until we can apply customizations needed for Omnia usage.

## Docker Helper

See the [tests](tests/README.md) for more information on getting setup for Docker testing.

The tests include a `make hubot` target that drops you into a running hubot shell repl.
